%
% File acl2015.tex
%
% Contact: car@ir.hit.edu.cn, gdzhou@suda.edu.cn
%%
%% Based on the style files for ACL-2014, which were, in turn,
%% Based on the style files for ACL-2013, which were, in turn,
%% Based on the style files for ACL-2012, which were, in turn,
%% based on the style files for ACL-2011, which were, in turn, 
%% based on the style files for ACL-2010, which were, in turn, 
%% based on the style files for ACL-IJCNLP-2009, which were, in turn,
%% based on the style files for EACL-2009 and IJCNLP-2008...

%% Based on the style files for EACL 2006 by 
%%e.agirre@ehu.es or Sergi.Balari@uab.es
%% and that of ACL 08 by Joakim Nivre and Noah Smith

\documentclass[11pt]{article}
\usepackage{acl2015}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{graphicx}
\graphicspath{ {figures/} }


%\setlength\titlebox{5cm}

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.


\title{A Hierarchical Structure for Document Summarisation}

\author{First Author \\
  Affiliation / Address line 1 \\
  Affiliation / Address line 2 \\
  Affiliation / Address line 3 \\
  {\tt email@domain} \\\And
  Second Author \\
  Affiliation / Address line 1 \\
  Affiliation / Address line 2 \\
  Affiliation / Address line 3 \\
  {\tt email@domain} \\}

\date{}

\begin{document}
\maketitle
\begin{abstract}
  Abstractive document summarisation generating a condensed version of
  a given document has attracted numerous attention. Most of previous works
  focus on mono-sentence summaries, while we, in this work, pay more attention
  to multi-sentence summaries. In order to deal with this complex task, we
  introduce a hierarchical structure to sequence-to-sequence model. This new
  model tries to encode a document in a hierarchical structure, which can be
  decoded in a similar way. In addition, two variants of our hierarchical models
  are proposed to tackle repetitive sentences or phrases during the decoding
  stage. We apply our model on CNN/Daily Mail dataset and evaluate its
  performance based on ROUGE system. Since our models fail to encode the
  source documents properly and make the attention mechanism futile, some
  meaningless and irrelevant summaries are produced.
\end{abstract}

\section{Introduction}

As an important challenge of natural language understanding, summarisation
has been paid a vast of attention. The core idea is to produce a headline or a
short summary consisting of a few sentences that preserves the core meaning
of an article or a passage. Based on how the condensed information is generated
, there are two summarisation approaches: {\em extractive} summarisation and 
{\em abstractive} summarisation. Extractive approaches aim to concatenate
some keywords or salient sentences appearing in the original text, whereas
abstractive tactics attempt to generate summaries from scratch without being
restricted to reuse phrases from the original text.

 Recently, due to its state-of-the-art performance, end-to-end models
 have been widely applied in many deep-learning based problems such as machine
 translation \cite{DBLP:journals/corr/BahdanauCB14}, speech recognition
 \cite{DBLP:journals/corr/BahdanauCSBB15} and video captioning
 \cite{DBLP:journals/corr/VenugopalanRDMD15}. Similar to the work done by
 Bahdanau \shortcite{DBLP:journals/corr/BahdanauCB14}, this task applies
 attentional Recurrent Neural Network (RNN) encoder-decoder model to abstractive
 summarisation. However, unlike machine translation (MT), the summary
 does not rely very much on the length of  the document in summarisation. Additionally,
 in translation, there exists a strong relation between the encoder and the decoder, but
 the exact one-to-one word level alignment between source and target is not that obvious
 for summarisation, especially for paragraph or document level. 
 
 From the linguistic perspective, a collection of syntactically- and semantically-ordered
 words consist of a complete and fluent sentence, and a coherent paragraph or document
 is formed by a branch of sentences \cite{Barzilay:2005:MLC:1219840.1219858,DBLP:journals/corr/cs-CL-0405039,elsner2008coreference,li2014model}. Recently, benefitting from RNN, sentence-level
 generation tasks have been widely explored, and show the capability of capturing local
 compositionality \cite{DBLP:journals/corr/SutskeverVL14,DBLP:journals/corr/LuongSLVZ14,chopra2016abstractive}.
 Li et al. \shortcite{DBLP:journals/corr/LiLJ15} demonstrated that RNN is capable of extracting
 the compositionality of discourse relations of higher-level text units (e.g. paragraph and documents)
 as well. Based on these experiments, we proposed a hierarchical model for document summarisation.
 
 The rest of the paper is organised as follows. In Section \ref{sec:2}, we briefly describe
 sequence-to-sequence LSTM model. Section \ref{sec:3} will depict our hierarchical LSTM model and its variants.
 Then, we present the results of our experiments on CNN/Dailymail dataset in Section \ref{sec:4}. Qualitative
 analysis of the output from our models also provided before concluding our work and stating
 some future directions. 
 
 \begin{figure*}[t]
\includegraphics[width=\textwidth,,height=9cm]{hier.png}
\centering
\caption{Hierarchical model}
\label{fig:1}
\end{figure*}
 
\section{Sequence-to-Sequence Model} \label{sec:2}
In natural language processing domain, sequence-to-sequence models attempts to directly
model the conditional probability $p(y|x)$ of converting a sequence of source words, 
$x_1,...,x_n$ to a target sequence, $y_1,...,y_m,$ where each
word $x_i \in \mathcal{V}$ and each word
$y_i \in \mathcal{U}.$ This conversion consists of three components:
(a) an {\em encoder} which transforms a series of words on source sentence into a
sequence of vectors: $h_1,...,h_n$, (b) a {\em decoder} which generates one target
word at a time and (c) an {\em attention} network which connects the encoder and
decoder, and decomposes the conditional probability as:
\begin{equation} \label{eq:1}
P(y|x;\theta)=\prod\limits_{j=1}^mp(y_j|y_{<j}, \textbf{x};\theta)
\end{equation}
The Equ. \ref{eq:1} can be modelled in RNN:
$$P(y|\textbf{x};\theta)=P_t=g_{\theta}(e_{y_{t}},h_t,c_t),$$
where $e_{y_{t}}$ is the word embedding of $y_{t},$ and $h_t$
is the hidden state of the RNN:
$$h_t=g_{\theta}(e_{y_{t}},h_{t-1},c_t).$$
Here $c_t$ is a context vector which can be obtained by following equation:
\begin{align*}
\epsilon_{tk}&=\textit{AttentionFunction}(h_{t-1}, e_{y_k})\\
\alpha_{tk}&=\frac{exp(\epsilon_{tk})}{\sum_{i=1}^{T_x}exp(\epsilon_{ti})}\\
c_t&=\sum_{k=1}^{T_x}\alpha_{tk}s_k
\end{align*}

Our $g$ can be functioned by LSTM \cite{Hochreiter:1997:LSM:1246443.1246450}:
\begin{align}\label{eq:2}
    \begin{split}
i_t&=\sigma(W_1e_{y_{t}}+W_2h_{t-1}+W_3c_t)\\
\tilde{c}_t&=tanh(W_4e_{y_{t}}+W_5h_{t-1}+W_6c_t)\\
f_t&=\sigma(W_7e_{y_{t}}+W_8h_{t-1}+W_9c_t)\\
o_t&=\sigma(W_{10}e_{y_{t}}+W_{11}h_{t-1}+W_{12}c_t)\\
c_t&=c_{t-1}\odot f_t+i_t\odot \tilde{c}_t\\
h_t&=c_t\odot o_t
   \end{split}
\end{align}
Operator $\odot$ refers to component-wise multiplication, and $W_{\{1,...,12\}}\in\mathbb{R}^{d\times d}$

The hidden vector $h_t$ is then fed through the softmax layer to produce the
predictive distribution formulated as:
$$p(y_t|y_{<t},x;\theta)=softmax(W_sh_t)$$

\section{Hierarchical Models} \label{sec:3}
In this section, we introduce our proposed hierarchical models for the document
summarisation.
\subsection{Notation}
Let $D$ denote a document, consisting of a sequence of $N_d$ sentences: $s_1,...,s_{N_d}.$
Each sentence $s$ is comprised of a sequence of tokens $s=\{x_1,...,x_{N_s}\},$ where $N_s$
denotes the length of the sentence. The word $x$ is associated with a $d$-dimensional embedding
$e_{w_x},$ where $e_{w_x} \in \mathcal{V}^{\lvert \mathcal{V} \rvert\times d}.$ 

Similarly, a summary $H$ is comprised of a sequence of sentences: $h_1,...,h_{N_h},$
with each sentence $h$ being composed of  a sequence of tokens $h=\{y_1,...,y_{N_k}\}.$
The word $y$ is embedded by $e_{w_y} \in \mathcal{U}^{\lvert \mathcal{U} \rvert\times d}.$ 

For simplicity, we define $\emph LSTM(h_{t-1},e_t, c_t)$ to be the LSTM operation
described in Equ. \ref{eq:2}. We also use the following notations for encoder and
decoder:
\begin{itemize}
  \item $h_t^w$ and $h_t^s$ denote hidden vectors from LSTM models, the subscripts
  indicate timestep $t$, the superscripts $w$ and $s$ specify word level and sentence
  level respectively. $h(enc)$ stands for encoding stage, while $h(dec)$ means decoding
  stage.
 \item $e_t^w$ and $e_t^s$ denote word-level and sentence-level embedding for word
 and sentence at timestep t.
\end{itemize}


\subsection{Model 1: Hierarchical Model}
The intuition of hierarchical model is based on cohesion and coherence among words
and sentences in a document.

\noindent
\textbf{Encoder}\space\space\space We first build a sequence of LSTM on word level for each sentence:
$$h_{t}^{w}(enc)=\textit{LSTM}_{encode}^{word}(e_t^{w_x},h_{t-1}^{w}(enc))$$
Then use the average of these word-level hidden vectors: $h_{1}^{w}(enc),...,h_{N_s}^{w}(enc)$
to represent a sentence embedding $e^{s}.$

To build representation $e^D$ for the current document $D$, another layer of LSTM is placed
on top of all sentence embeddings, computing representations sequentially for each timestep:
$$h_{t}^{s}(enc)=\textit{LSTM}_{encode}^{sentence}(e_t^{s},h_{t-1}^{s}(enc))$$
The last hidden vector $h_{N_d}^s$ at sentence-level encoder is used to represent the entire
document.

\noindent
\textbf{Decoder} \space\space\space Like encoding stage, decoder also manipulates a hierarchical structure. Conversely,
decoding stage starts at sentence level:
$$h_{t}^{s}(dec)=\textit{LSTM}_{decode}^{sentence}(e_t^{s},h_{t-1}^{s}(dec))$$
The initial time step $h_0^{s}=e^D$ is the end-to-end output from the encoding procedure.
$h_t^s(dec)$ is used as the initial hidden state fed into $\textit{LSTM}_{decode}^{word}$ for
subsequently predicting tokens within sentence $t+1$. Afterwards, the word embedding $e_t^{w_y}$
and the earlier hidden vector $h_{t-1}^{w}(dec)$ are combined together as inputs of
$\textit{LSTM}_{decode}^{word}.$ This procedure can be summarised as follows:
\begin{align*}
h_t^{w}(dec)&=\textit{LSTM}_{decode}^{word}(e_t^{w_y}, h_{t-1}^w(dec))\\
p(w|\cdot)&=softmax(h_t^w(dec))
\end{align*}

The last $\textit{LSTM}_{decode}^{word}$ hidden vector is used to represent the
current sentence. Then this hidden vector is passed to $\textit{LSTM}_{decode}^{sentence}$,
and combined with $h_t^s$ for the acquisition of $h_{t+1}^s.$ Details are shown in Figure \ref{fig:1}.

\noindent
\textbf{Attention}\space\space\space Like MT task, we also adopt attention schema, linking the current decoding
sentence with entire encoding sentences to retain the most responsible input sentences.

Let $H=\{h_1^s(enc),...,h_{N_d}(enc)\}$ be the collection of sentence-level hidden vectors
computed by $\textit{LSTM}_{decode}^{sentence}.$ During decoding stage, the context vector
$c_t$ in Equ. \ref{eq:2} is characterised by follows: 
\begin{align*}
\epsilon_{i}&=U^Tf(W_1\cdot h_{t-1}^{s}(dec)+W_2\cdot h_i^{s}(enc))\\
\alpha_i&=\frac{exp(\epsilon_i)}{\sum_{i^{\prime}}exp(\epsilon_{i}^{\prime})}\\
c_t&=\sum_{i=1}^{N_D}\alpha_{i}h_{i}^{s}(enc)
\end{align*}
where $W_1, W_2 \in \mathbb{R}^{d\times d}, U\in\mathbb{R}^{d\times1}.$

\begin{figure*}[t]
\includegraphics[width=\textwidth,,height=9cm]{intra.png}
\centering
\caption{Hierarchical model with intra-attention}
 \label{fig:2}
\end{figure*}

\subsection{Model 2: Hierarchical Model with Fertility} \label{ssec:3.3}
Since attention mechanism does not directly encode information as to which source
words/ fragments have been focused in the earlier timesteps, repeating phenomenon
is inevitable and prevalent among attention-driven tasks \cite{DBLP:journals/corr/SankaranMAI16}.
Inspired by \cite{DBLP:journals/corr/CohnHVYDH16}, we incorporate fertility into the attentional model. Fertility is a central
component in the IBM models 3-5 \cite{Brown:1993:MSM:972470.972474}. We use two tactics to constrain
an encoding sentence from being attended multiple times.

\noindent
\textbf{Local fertility}\space\space\space We introduce additional features to augment the attentional input:
\begin{align*}
\epsilon_{i}&=U^Tf(\ldots+W_3\cdot \xi(\epsilon_{<t};i))\\
\xi(\epsilon_{<t};i)&=\Big[\sum_{t^{\prime}<t}\epsilon_{t^{\prime},i-k},...,
\sum_{t^{\prime}<t}\epsilon_{t^{\prime},i},...,\sum_{t^{\prime}<t}\epsilon_{t^{\prime},i+1}\Big]^{T}
\end{align*}
where $W_3\in\mathbb{R}^{d\times(2k+1)}.$ These sums represent the total alignment score
for the surrounding encoding sentences. A sentence which already has several alignments
can be omitted, whereas sentences that tend to need high fertility will be involved.

\noindent
\textbf{Global fertility}\space\space\space A more explicit technique for incorporating fertility is to include this as
a modelling constraint. This constraint can be parameterised by the following Equ.:
\begin{align*}
p(f_i\vert s,i)=\mathcal{N}(\mu(h_i^s(enc)),\sigma^2(h_i^s(enc)))
\end{align*}
where $f_i$ is defined as: $f_i=\sum_{t=1}^{N_h}\epsilon_{t,i}.$ And this is added to the training
objective as an additional term: $\sum_{i}log\,p(f_i\vert s,i),$ for each training instance.

\subsection{Model 3: Hierarchical Model with Long Short-Term Memory-Network} \label{ssec:3.4}
Theoretically LSTMs are capable of maintaining unbounded memory, but this
assumption, due to the constraint of the memory size, may fail in practice \cite{DBLP:journals/corr/ChengDL16}.
Hence during inference stage, the same sentences or phrases often gets repeated in the
summary \cite{DBLP:journals/corr/NallapatiXZ16}. We incorporate intra-attention \cite{DBLP:journals/corr/ChengDL16} into our hierarchical model
to fix this problem by encourage our model to "remember" the words it has already produced
in the past, and to omit the encoding sentences it has attended simultaneously.

Let $x_t$ denote the current input (either $e_t^{w}$ or $e_t^{s}$). At time step t, model
computes the relation between $x_t$ and $x_1,...,x_{t-1}$ through $h_1,...,h_{t-1}$ with
an attention layer:
\begin{align*}
a_i^t&=v^Ttanh(W_hh_i+W_xx_i)\\
s_i^t&=softmax(a_i^t)
\end{align*} 
This produces a probability distribution over the previous inputs, which can be used to
compute an adaptive summary of content vectors and hidden vectors respectively:
\[\left[
  \begin{array}{c}
    \tilde{h}_t\\
    \tilde{c}_t
  \end{array}
\right] = \sum_{i=1}^{t-1}s_i^t\cdot
\left[
  \begin{array}{c}
    h_i\\
    c_i
  \end{array}
\right]
\]
Then apply $\tilde{c}_t$ and $\tilde{h}_t$ on LSTM:
\begin{align*}
\left[
  \begin{array}{c}
    i_t\\
    f_t\\
    o_t\\
    \hat{c}_t
  \end{array}
\right] &= 
\left[
  \begin{array}{c}
    \sigma\\
    \sigma\\
    \sigma\\
    tanh
  \end{array}
\right]W\cdot[\tilde{h_t},x_t]\\
c_t&=f_t\odot\tilde{c}_t+i_t\odot\hat{c}_t\\
h_t&=o_t\odot tanh(c_t)
\end{align*}

\begin{table*}[t]
\begin{center}
 \begin{tabular}{||c c c c c c|c c c c c||} 
  \hline
 &\multicolumn{5}{c|}{Sentence Level} &\multicolumn{5}{c||}{Word Level}\\
 \hline
  & Min & Max & Avg & $>$Avg (\%)& $>$30 (\%) & Min & Max & Avg & $>$Avg (\%) & $>$40 (\%) \\ [0.5ex] 
 \hline\hline
 training set & 2 & 265 & 29.74 & 1 & 1 & 1 & 726 & 23.04 & 47 & 1\\ 
 \hline
 val set  & 1 & 172 & 27.1 & 1 & 1 & 1 & 983 & 27.7 & 40 & 1\\
 \hline
 test set & 1 & 129 & 27.6 & 1& 1 & 1 & 788 & 27.5 & 40 & 1\\ [1ex] 
 \hline
\end{tabular}
\end{center}
\caption{Statistics on documents. Sentence level means no. of sentences of each document, while
word level indicates no. of words of each sentence}
\label{table:1}
\end{table*}

\begin{table*}[t]
\begin{center}
 \begin{tabular}{||c c c c c c|c c c c c||} 
  \hline
 &\multicolumn{5}{c|}{Sentence Level} &\multicolumn{5}{c||}{Word Level}\\
 \hline
  & Min & Max & Avg & $>$Avg(\%) & $>$5 (\%) & Min & Max & Avg & $>$Avg(\%) & $>$25(\%) \\ [0.5ex] 
 \hline\hline
 training set & 1 & 169 & 3.72 & 20 & 10 & 1 & 775 & 15.96 & 32 & 10\\ 
 \hline
 val set  & 1 & 34 & 4.12 & 33 & 14 & 1 & 125 & 13.5 & 40 & 3\\
 \hline
 test set & 1 & 47 & 3.97 & 60 & 9 & 1 & 141 & 13.8 & 40 & 4\\ [1ex] 
 \hline
\end{tabular}
\end{center}
\caption{Statistics on summaries. Sentence level means no. of sentences of each summary, while
word level indicates no. of words of each sentence}
\label{table:2}
\end{table*}

\subsection{Training and Testing}
Given a corpus $\mathcal{S}=\{(x^i,y^i)\}_{i=1}^S$ of S document-summary
pairs, parameters $\theta$ are estimated by minimising the negative conditional log
likelihood of outputs given inputs, similar to standard sequence-to-sequence
models. 
\begin{align}\label{eq:3}
\mathcal{L}&=-\sum_{i=1}^{S}\sum_{j=1}^{N_h}\sum_{k=1}^{N_k}log\,P(y_{j,k}^{i}|y_{1,1}^i,...,y_{j,k-1}^i,x^i;\theta)
\end{align}
where $N_h$ is the amount of sentences of a summary, and $N_k$ is the
length of each sentence.

For testing, we adopt a greedy strategy with no beam search. For a given
document $D, e_D$ is first obtained by a well-trained $LSTM_{encode}$.
Then, $e_D$ is fed into $LSTM_{decode}$ until $LSTM_{decode}^{word}$
generates a $<$eoh$>$ token indicating the termination of this decoding or 
the length of the generated summary reaches an upper bound.

\section{Experiments} \label{sec:4}
\subsection{Datasets and Evaluations}
We implement our hierarchical model on CNN/Daily Mail corpus, which is also
used in \cite{DBLP:journals/corr/NallapatiXZ16}. Unlike Gigaword and DUC corpus, consisting of only
one sentence in each summary, CNN/Daily Mail corpus is comprised of multi-sentence
summaries, which is naturally suitable for our hierarchical model. This dataset has
305,320 news-summary pairs, with 286,952 training pairs, 13,368 validation pairs
and 5000 test pairs. The source documents in training set have 766 words spanning 29.74 sentences on an
average while the summaries consist of 53 words and 3.72 sentences. The
dataset is released in two versions: one is anonymised, whose entity names
are replaced with document-specific integer-ids beginning from 0, another one
consists of actual entity names. Since the vocabulary size is smaller in the
anonymised version, we used it in all our experiments. Based on statistics in
Table \ref{table:1} and Table \ref{table:2}, we limited the source documents
and target summaries to at most 30 sentences with 40 words per
sentence and 5 sentences with 25 words per sentence respectively. In addition,
we also constrained the source vocabulary size to 85k, and the target vocabulary
size to 60k. Our evaluation is based on three variants of ROUGE \cite{lin2004rouge,Lin:2003:AES:1073445.1073465},
namely, ROUGE-1 (unigrams), ROUGE-2 (bigrams), and ROUGE-L
(longest-common substring).

\begin{table*}[ht]
\begin{center}
\begin{tabular}{c|c|c|c|c|c|c|c|c}
    \hline
    \multirow{2}{*}{model}&\multirow{2}{*}{batch} & \multirow{2}{*}{embedding}&\multicolumn{3}{c|}{Sentence Level} &\multicolumn{3}{c}{Word Level}\\
    \cline{4-9}
    & & &hidden & layers & dropout & hidden &layers &dropout\\
    \hline
    hier        & 32 &100 & 300 &1 & 0.7 & 300 & 1 &0.7\\
    \hline
    hier\_fert & 32 &150 & 600 &1 & 0.9 & 600 & 1 &0.9\\
    \hline
    hier\_intra & 32 &150 & 300 &1 & 0.5 & 300 & 1 &0.5\\
    \hline
\end{tabular}
\end{center}
\caption{Final hyper-parameters}
\label{tab:3}
\end{table*}

\subsection{Architectural Choices}
To optimise our loss (Equ. \ref{eq:3}) we used Adam \cite{DBLP:journals/corr/KingmaB14} for training,
with initial learning rate of 0.001. Gradient clipping is adopted by scaling gradients when
the global norm exceeded a threshold of 5. During the training we adjust our hyper-parameters
by measuring the perplexity of the summaries in the validation set. We chose
hyper-parameters based on a grid search and picked the one which gave the
best perplexity on the validation set. Our final hyper-parameters are listed in Table \ref{tab:3}.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c|c|c|}
    \hline
    Model & Rouge-1 & Rouge-2 & Rouge-L\\
    \hline
    hier        &21.13        &   2.83     &  19.78\\
    \hline
    hier\_fert & 21.96      &   2.91     &  20.04\\
    \hline
    hier\_intra &23.14     &   3.13     &  20.93\\
    \hline
    seq2seq &*\textbf{24.14}       &   *\textbf{5.47}     &  *\textbf{23.12}\\
    \hline
\end{tabular}
\end{center}
\caption{Performance of various models on CNN/Daily Mail test set using full-length Rouge-F1.
Bold faced numbers indicate best performing system.}
\label{tab:4}
\end{table}

\subsection{Results}
We also implemented the basic attention encoder-decoder serving as our baseline. Results of
all models are displayed in Table \ref{tab:4}. Although the hierarchical model with intra-attention described
in Sec.\ref{ssec:3.4} outperforms all its variants, all hierarchical modes are worse than the baseline, which
embarrasses us and makes our work useless.

\begin{table*}[!htbp]
\begin{center}
\begin{tabular}{|l|}
    \hline
\textbf{Source Document}\\
\hline
\parbox{\textwidth}{@entity1 's players were the latest to face the wrath of angry fans following thursday 's capitulation
against @entity6 rivals @entity5 . $<$eos$>$ @entity8 pundit @entity7 tweeted that @entity6 newspaper
@entity9 reported @entity1 fans had demanded players take off their kits as they were n't worthy of
wearing them . $<$eos$>$ the @entity13 had offered the last realistic chance of silverware this season to
the @entity17 following a miserable run of form , with the team falling 14 points behind @entity8 leaders
@entity23 and at risk of losing second place to city rivals @entity26 . $<$eos$>$ @entity1 midfielder$<$eos$>$
@entity27 speaks with fans after crashing out of the @entity13 @entity1 captain @entity29 also spoke
to fans despite being an unused substitute @entity1 $<$eos$>$ ' ultras ' had walked out of the stadium in
protest following a miserable run of form fans are upset as @entity1 are at risk of losing second place to
city rivals @entity26 but @entity1 conceded three goals in the opening 22 minutes as @entity5 completed
a 4 - 1 victory on aggregate to book a place in the last eight . $<$eos$>$ @entity46 converted a retaken
penalty in the ninth minute , after a number of players encroached into the box for the first attempt .
$<$eos$>$ @entity52 capitalised on a goalkeeping error to make it 2 - 0 in the 18th minute and @entity54 headed in when unmarked four minutes later . $<$eos$>$ a late sending - off for @entity57 for a second yellow card capped a dispiriting night which saw about a hundred of the club 's @entity60 leave the @entity62 after 30 minutes . $<$eos$>$ @entity1 's players walked out for the second half to a near - empty stadium supporters left the stadium in protest after @entity1 conceded three goals in 22 minutes @entity $<$eos$>$ 1 supporters left off flares during an @entity13 round of 16 second leg @entity5 fans celebrate booking place in the last eight of the @entity13 @entity1 captain @entity29 , goalkeeper @entity75 and midfielder @entity27 led the rest of the team 's players over to talk to fans after match . $<$eos$>$ the scenes were reminiscent of the players of @entity84 sides @entity85 and @entity86 apologising to their supporters following a dismal run this season . $<$eos$>$ @entity85 defender @entity90 talks to supporters after defeat by @entity93 in february @entity94 critical of individual errors $<$eos$>$}\\
\hline
\textbf{Ground truth Summary}\\
\hline
\parbox{\textwidth}{@entity1 crashed out of @entity13 following home defeat by @entity5 $<$eos$>$ captain @entity29 led players to talk to fans after dismal display $<$eos$>$ the @entity17 at risk of losing second place in @entity8 to rivals @entity26}\\
\hline
\textbf{hier}\\
\hline
\parbox{\textwidth}{@entity1 fans were sent off for the match against @entity4 $<$eos$>$ the match was cancelled after the match was cancelled $<$eos$>$ @entity1 were beaten by the group of the group 's group stage $<$eos$>$ @entity1 have been on the pitch for the first time since the last season $<$eos$>$ @entity1 have been on the pitch for the first time since the last season}\\
\hline
\textbf{hier\_fert}\\
\hline
\parbox{\textwidth}{@entity1 fans were furious at the fans after the game $<$eos$>$ the @entity1 fans were angered by the decision to cancel the game $<$eos$>$ @entity1 's side were beaten 2 - 1 by @entity7 in the semi-finals}\\
\hline
\textbf{hier\_intra}\\
\hline
\parbox{\textwidth}{@entity1 have lost their opening game against @entity5 $<$eos$>$ @entity1 have lost their opening game against @entity5 $<$eos$>$ the @entity1 have lost their opening game in the league}\\
\hline
\textbf{seq2seq}\\
\hline
\parbox{\textwidth}{@entity1 's players were forced to win the @entity6 's first half $<$eos$>$ the @entity1 boss @entity0 said the fans were angry to the fans $<$eos$>$ the @entity1 boss @entity0 said the fans were angry to the fans $<$eos$>$ the @entity1 boss @entity0 said the fans were angry to the fans $<$eos$>$ the @entity1 boss @entity0 said the fans were angry to the fans $<$eos$>$ the @entity1 boss @entity0 said the fans were angry to the fans $<$eos$>$ the @entity1 boss @entity0 said the fans were angry to the fans $<$eos$>$ the @entity1 boss @entity0 said the fans were angry}\\
\hline
\end{tabular}
\end{center}
\caption{Comparison of gold truth summary with summaries from various systems. Named entities are anonymised by the preprocessing script. The "$<$eos$>$" tags represent the boundary between two sentences}
\label{tab:5}
\end{table*}

\section{Qualitative Analysis}
Table \ref{tab:5} presents a piece of news, its gold truth summary and system outputs from various models.
Although summaries generated by \textit{hier\_intra} and \textit{seq2seq} differ from the gold truth, they tend to
capture certain salient information from the source document. However, both \textit{hier} and \textit{hier\_fert}
misunderstand the source document and generate some meaningful and irrelevant sentences. Upon visual inspection
of the system outputs, our hierarchical models generated a vast of poor quality summaries, which misinterpret the source
documents. Clearly, these models lack the capability of capturing the meaning of complex sentences.

We also illustrate the alignment information between the encoder and decoder in Figure \ref{fig:3}. The alignment scores of encoding sentence
vectors are even across all decoding sentence vectors. Based on this observation, we believe our hierarchical models cannot
encode proper sentence level information, which caused the attention mechanism ineffective. Consequently, the decoder is
unable to select the relevant and crucial information from the encoder. This might also be an explanation that even we applied
two strategies on the vanilla hierarchical model, the same sentence or phrase still gets repeated in the summary.
\begin{figure}[t]
\includegraphics[scale=0.25]{alignment.png}
\centering
\caption{Alignment between encoding sentence vectors and decoding sentence vectors. The x-axis and y-axis
correspond to the decoding vectors and encoding vectors respectively. Each pixel show the weight $\alpha_{ij}$ of the
notation of the $j$-th target sentence for $i$-th source sentence in grayscale (0: black, 1: white)}
 \label{fig:3}
\end{figure}

\section{Conclusion and Future Work}
In this work, we apply the hierarchical model for the task of multi-sentence abstractive summarisation. Additionally
we incorporate two novel ideas into our hierarchical model as well. However, all these models failed to improve the
summarisation performance. After analysing the alignment information, we raised a potential issue which might cause
the failure of our work. Despite our models have a poor performance, as part of our future work, we plan to build a
sophisticated model which is capable of encoding the elaborate and structural text units.


% include your own bib file like this:
\bibliography{acl2015}
\bibliographystyle{acl}

%\begin{thebibliography}{}
%
%\bibitem[\protect\citename{Aho and Ullman}1972]{Aho:72}
%Alfred~V. Aho and Jeffrey~D. Ullman.
%\newblock 1972.
%\newblock {\em The Theory of Parsing, Translation and Compiling}, volume~1.
%\newblock Prentice-{Hall}, Englewood Cliffs, NJ.
%
%\bibitem[\protect\citename{{American Psychological Association}}1983]{APA:83}
%{American Psychological Association}.
%\newblock 1983.
%\newblock {\em Publications Manual}.
%\newblock American Psychological Association, Washington, DC.
%
%\bibitem[\protect\citename{{Association for Computing Machinery}}1983]{ACM:83}
%{Association for Computing Machinery}.
%\newblock 1983.
%\newblock {\em Computing Reviews}, 24(11):503--512.
%
%\bibitem[\protect\citename{Chandra \bgroup et al.\egroup }1981]{Chandra:81}
%Ashok~K. Chandra, Dexter~C. Kozen, and Larry~J. Stockmeyer.
%\newblock 1981.
%\newblock Alternation.
%\newblock {\em Journal of the Association for Computing Machinery},
%  28(1):114--133.
%
%\bibitem[\protect\citename{Gusfield}1997]{Gusfield:97}
%Dan Gusfield.
%\newblock 1997.
%\newblock {\em Algorithms on Strings, Trees and Sequences}.
%\newblock Cambridge University Press, Cambridge, UK.
%
%\end{thebibliography}

\end{document}
